const deuses = require("./deuses.js").gods;

let listDeuses = []
for (deus of deuses) {
    listDeuses.push(`${deus.name} (${deus.class})`)
}

console.log(listDeuses)